package by.itstep.trainer.manager.stashinsky.service.impl;

import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.stashinsky.entity.AppointmentEntity;
import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;
import by.itstep.trainer.manager.stashinsky.mapper.AppointmentEntityMapper;
import by.itstep.trainer.manager.stashinsky.repository.AppointmentEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.AppointmentEntityRepositoryImpl;
import by.itstep.trainer.manager.stashinsky.repository.TrainerEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.TrainerEntityRepositoryImpl;
import by.itstep.trainer.manager.stashinsky.service.AppointmentService;

import java.util.List;

public class AppointmentServiceImpl implements AppointmentService {

    private final AppointmentEntityRepository appointmentRepository = new AppointmentEntityRepositoryImpl();
    private final AppointmentEntityMapper mapper = new AppointmentEntityMapper();
    private final TrainerEntityRepository trainerRepository = new TrainerEntityRepositoryImpl();

    @Override
    public List<AppointmentPreviewDto> findAll() {

        List<AppointmentPreviewDto> dtos = mapper.mapToDtoList(appointmentRepository.findAll());

        System.out.println("Appointments found: " + dtos);
        return dtos;
    }

    @Override
    public AppointmentFullDto findById(Long id) {

        AppointmentFullDto appointment = mapper.mapToDto(appointmentRepository.findById(id));

        System.out.println("Appointment : " + appointment);

        return appointment;
    }

    @Override
    public AppointmentFullDto create(AppointmentCreateDto createAppointment) {

        TrainerEntity trainer = trainerRepository.findBiId(createAppointment.getTrainerId());

        AppointmentEntity appointmentToSave = mapper.mapToEntity(createAppointment,trainer);
        AppointmentEntity created = appointmentRepository.create(appointmentToSave);

        AppointmentFullDto createDto = mapper.mapToDto(created);
        System.out.println("Appointment created");

        return createDto;
    }

    @Override
    public AppointmentFullDto update (AppointmentUpdateDto updateAppointment) {

        TrainerEntity trainer = trainerRepository.findBiId(updateAppointment.getTrainerId());

        AppointmentEntity toUpdate = mapper.mapToEntity(updateAppointment,trainer);
        AppointmentEntity appointmentInDB = appointmentRepository.findById(updateAppointment.getId());

        toUpdate.setAppointmentTime(appointmentInDB.getAppointmentTime());
        toUpdate.setAppointmentDate(appointmentInDB.getAppointmentDate());

        AppointmentEntity updated = appointmentRepository.update(toUpdate);
        AppointmentFullDto updateDto = mapper.mapToDto(updated);
        System.out.println("Appointment was updated");

        return updateDto;
    }

    @Override
    public void deleteById(Long id) {

        appointmentRepository.deleteById(id);
        System.out.println("Appointment was deleted");

    }
}
