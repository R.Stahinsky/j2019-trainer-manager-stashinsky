package by.itstep.trainer.manager.stashinsky.service.impl;

import by.itstep.trainer.manager.stashinsky.dto.admin.AdminCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminUpdateDto;
import by.itstep.trainer.manager.stashinsky.entity.AdminEntity;
import by.itstep.trainer.manager.stashinsky.mapper.AdminEntityMapper;
import by.itstep.trainer.manager.stashinsky.repository.AdminEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.AdminEntityRepositoryImpl;
import by.itstep.trainer.manager.stashinsky.service.AdminService;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    private final AdminEntityRepository adminRepository = new AdminEntityRepositoryImpl();
    private final AdminEntityMapper mapper = new AdminEntityMapper();


    @Override
    public List<AdminPreviewDto> findAll() {

        List<AdminPreviewDto> dtos = mapper.mapToDtoList(adminRepository.findAll());

        System.out.println("Preview found: " + dtos);

        return dtos;
    }

    @Override
    public AdminFullDto findById(Long id) {

        AdminFullDto admin = mapper.mapToDto(adminRepository.findById(id));

        System.out.println("Admin: " + admin);

        return admin;
    }


    @Override
    public AdminFullDto create(AdminCreateDto createAdmin) {

        AdminEntity adminToSave = mapper.mapToEntity(createAdmin);
        AdminEntity created = adminRepository.create(adminToSave);

        AdminFullDto createDto = mapper.mapToDto(created);

        System.out.println("Admin created: " + created);

        return createDto;
    }

    @Override
    public AdminFullDto update(AdminUpdateDto updateAdmin) {

        AdminEntity toUpdate = mapper.mapToEntity(updateAdmin);
        AdminEntity adminInDB = adminRepository.findById(updateAdmin.getId());

        toUpdate.setRole(adminInDB.getRole());

        AdminEntity updated = adminRepository.update(toUpdate);
        AdminFullDto updateDto = mapper.mapToDto(updated);
        System.out.println("Admin was updated: " + updated);

        return updateDto;
    }

    @Override
    public void deleteById(Long id) {

        adminRepository.deleteById(id);
        System.out.println("Admin was deleted");
    }
}
