package by.itstep.trainer.manager.stashinsky.dto.comment;

import by.itstep.trainer.manager.stashinsky.enums.CommentStatus;
import lombok.Data;

@Data
public class CommentFullDto {
    private Long id;
    private String userName;
    private String userLastName;
    private String message;
    private String email;
    private int trainerPoints;
    private CommentStatus status;
    private Long trainerId;

}
