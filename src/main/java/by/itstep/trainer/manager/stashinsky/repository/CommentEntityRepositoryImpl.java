package by.itstep.trainer.manager.stashinsky.repository;

import by.itstep.trainer.manager.stashinsky.entity.AdminEntity;
import by.itstep.trainer.manager.stashinsky.entity.CommentEntity;
import by.itstep.trainer.manager.stashinsky.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentEntityRepositoryImpl implements CommentEntityRepository {
    @Override
    public CommentEntity findById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        CommentEntity foundCommentEntity = em.find(CommentEntity.class, id);

        em.close();
        System.out.println("Found comment: " + foundCommentEntity);
        return foundCommentEntity;
    }

    @Override
    public List<CommentEntity> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<CommentEntity> foundComments = em.createNativeQuery("SELECT * FROM comment", CommentEntity.class).getResultList();

        em.close();
        System.out.println("Found: " + foundComments.size() + "comments");

        return foundComments;
    }

    @Override
    public List<CommentEntity> findByUserName(String userName) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<CommentEntity> foundByName = em.createNativeQuery("SELECT * FROM comment WHERE user_name = '" + userName + "'", CommentEntity.class).getResultList();

        em.close();
        System.out.println("Found By Name: " + foundByName);
        return foundByName;
    }

    @Override
    public List<CommentEntity> findByUserLastName(String userLastName) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<CommentEntity> foundByLastName = em.createNativeQuery("SELECT * FROM comment WHERE user_last_name = '" + userLastName + "'", CommentEntity.class).getResultList();

        em.close();
        System.out.println("Found By Last name: " + foundByLastName);
        return foundByLastName;
    }

    @Override
    public List<CommentEntity> findByEmail(String email) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<CommentEntity> foundCommentByEmail = em.createNativeQuery("SELECT * FROM comment WHERE email ='" + email + "'", CommentEntity.class).getResultList();

        em.close();
        System.out.println("FOUND BY EMAIL: " + foundCommentByEmail);
        return foundCommentByEmail;    }

    @Override
    public CommentEntity create(CommentEntity commentEntity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(commentEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment is created");
        return commentEntity;

    }

    @Override
    public CommentEntity update(CommentEntity commentEntity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(commentEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment is updated");
        return commentEntity;    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        CommentEntity foundComment = em.find(CommentEntity.class, id);
        em.remove(foundComment);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM comment").executeUpdate();

        em.getTransaction().commit();
        em.close();
        System.out.println("all comments was deleted");


    }
}
