package by.itstep.trainer.manager.stashinsky.enums;

public enum Role {
    ADMIN,
    SUPERUSER
}
