package by.itstep.trainer.manager.stashinsky.dto.trainer;

import by.itstep.trainer.manager.stashinsky.entity.CommentEntity;
import lombok.Data;

import java.util.List;

@Data
public class TrainerPreviewDto {

    private Long id;
    private String name;
    private String lastName;
    private int workExperience;
    private String achievements;
    private String imgUrl;





}
