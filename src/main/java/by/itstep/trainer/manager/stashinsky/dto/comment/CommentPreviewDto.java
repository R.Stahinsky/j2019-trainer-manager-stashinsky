package by.itstep.trainer.manager.stashinsky.dto.comment;

import lombok.Data;

@Data
public class CommentPreviewDto {

    private Long id;
    private String userName;
    private String message;

}
