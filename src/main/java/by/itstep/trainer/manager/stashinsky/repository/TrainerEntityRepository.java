package by.itstep.trainer.manager.stashinsky.repository;

import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;

import java.util.List;

public interface TrainerEntityRepository {

    TrainerEntity findBiId (Long id);

    List<TrainerEntity> findAll ();

    List<TrainerEntity> findByName (String name);

    List<TrainerEntity> findByLastName (String lastName);

    List<TrainerEntity> findByEmail (String email);

    List<TrainerEntity> findByExperience (int workExperience);

    TrainerEntity create (TrainerEntity trainerEntity);

    TrainerEntity update (TrainerEntity trainerEntity);

    void deleteById (Long id);

    void deleteAll();
}
