package by.itstep.trainer.manager.stashinsky.repository;

import by.itstep.trainer.manager.stashinsky.entity.AdminEntity;
import by.itstep.trainer.manager.stashinsky.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class AdminEntityRepositoryImpl implements AdminEntityRepository {

    @Override
    public AdminEntity findById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        AdminEntity foundAdminEntity = em.find(AdminEntity.class, id);

        em.close();
        System.out.println("Found admin: " + foundAdminEntity);
        return foundAdminEntity;
    }

    @Override
    public List<AdminEntity> findAll() {

        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AdminEntity> foundAdminList = em.createNativeQuery("SELECT * FROM admin", AdminEntity.class).getResultList();

        em.close();
        System.out.println("Found " + foundAdminList.size() + "admins");
        return foundAdminList;
    }

    @Override
    public List<AdminEntity> findByName(String name) {

        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AdminEntity> foundAdminByName = em.createNativeQuery("SELECT * FROM admin WHERE name ='" + name + "'", AdminEntity.class).getResultList();

        em.close();
        System.out.println("FOUND BY NAME: " + foundAdminByName);
        return foundAdminByName;
    }

    @Override
    public List<AdminEntity> findByLastName(String lastName) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AdminEntity> foundAdminByLastName = em.createNativeQuery("SELECT * FROM admin WHERE last_name ='" + lastName + "'", AdminEntity.class).getResultList();

        em.close();
        System.out.println("FOUND BY LAST NAME: " + foundAdminByLastName);
        return foundAdminByLastName;
    }

    @Override
    public List<AdminEntity> findByEmail(String email) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AdminEntity> foundAdminByEmail = em.createNativeQuery("SELECT * FROM admin WHERE email ='" + email + "'", AdminEntity.class).getResultList();

        em.close();
        System.out.println("FOUND BY EMAIL: " + foundAdminByEmail);
        return foundAdminByEmail;
    }

    @Override
    public AdminEntity create(AdminEntity adminEntity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(adminEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Admin is created");
        return adminEntity;
    }

    @Override
    public AdminEntity update(AdminEntity adminEntity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(adminEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Admin is updated");
        return adminEntity;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        AdminEntity foundAdmin = em.find(AdminEntity.class, id);
        em.remove(foundAdmin);

        em.getTransaction().commit();
        em.close();
        System.out.println("admin was deleted by id");
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM admin").executeUpdate();

        em.getTransaction().commit();
        em.close();
        System.out.println("all admins was deleted");


    }
}
