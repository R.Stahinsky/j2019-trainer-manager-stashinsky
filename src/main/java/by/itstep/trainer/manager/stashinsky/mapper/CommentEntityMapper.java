package by.itstep.trainer.manager.stashinsky.mapper;

import by.itstep.trainer.manager.stashinsky.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.stashinsky.entity.CommentEntity;
import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class CommentEntityMapper {

    public CommentEntity mapToEntity (CommentCreateDto createDto, TrainerEntity trainer) {

        CommentEntity comment = new CommentEntity();

        comment.setUserName(createDto.getUserName());
        comment.setUserLastName(createDto.getUserLastName());
        comment.setMessage(createDto.getMessage());
        comment.setTrainerPoints(createDto.getTrainerPoints());
        comment.setTrainer(trainer);

        return  comment;
    }

    public  CommentEntity mapToEntity (CommentUpdateDto updateDto){

        CommentEntity comment = new CommentEntity();

        comment.setId(updateDto.getId());
        comment.setMessage(updateDto.getMessage());
        comment.setTrainerPoints(updateDto.getTrainerPoints());

        return  comment;
    }

    public List<CommentPreviewDto> mapToDtoList (List<CommentEntity> entityList) {
        List<CommentPreviewDto> dtoList = new ArrayList<>();

        for (CommentEntity comment : entityList) {

            CommentPreviewDto dto = new CommentPreviewDto();

            dto.setId(comment.getId());
            dto.setUserName(comment.getUserName());
            dto.setMessage(comment.getMessage());

            dtoList.add(dto);
        }
        return dtoList;
    }

    public CommentFullDto mapToDto (CommentEntity commentEntity) {

        if (commentEntity == null){
            return null;
        } else {

            CommentFullDto dto = new CommentFullDto();

            dto.setId(commentEntity.getId());
            dto.setUserName(commentEntity.getUserName());
            dto.setUserLastName(commentEntity.getUserLastName());
            dto.setMessage(commentEntity.getMessage());
            dto.setEmail(commentEntity.getEmail());
            dto.setTrainerPoints(commentEntity.getTrainerPoints());
            dto.setStatus(commentEntity.getStatus());

            return dto;
        }
    }

}
