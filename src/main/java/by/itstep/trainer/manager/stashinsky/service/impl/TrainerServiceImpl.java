package by.itstep.trainer.manager.stashinsky.service.impl;

import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;
import by.itstep.trainer.manager.stashinsky.mapper.TrainerEntityMapper;
import by.itstep.trainer.manager.stashinsky.repository.TrainerEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.TrainerEntityRepositoryImpl;
import by.itstep.trainer.manager.stashinsky.service.TrainerService;

import java.util.List;

public class TrainerServiceImpl implements TrainerService {

    private final TrainerEntityRepository trainerRepository = new TrainerEntityRepositoryImpl();
    private final TrainerEntityMapper mapper = new TrainerEntityMapper();

    @Override
    public List<TrainerPreviewDto> findAll() {

        List<TrainerPreviewDto> dtos = mapper.mapToDtoList(trainerRepository.findAll());

        System.out.println("Trainers found: "+ dtos);

        return dtos;
    }

    @Override
    public TrainerFullDto findById(Long id) {

        TrainerFullDto trainer = mapper.mapToDto(trainerRepository.findBiId(id));

        System.out.println("Trainer: "+ trainer);

        return trainer;
    }

    @Override
    public TrainerFullDto create(TrainerCreateDto createTrainer) {

        TrainerEntity trainerToSave = mapper.mapToEntity(createTrainer);
        TrainerEntity created = trainerRepository.create(trainerToSave);

        TrainerFullDto createDto = mapper.mapToDto(created);

        return createDto;
    }

    @Override
    public TrainerFullDto update(TrainerUpdateDto updateTrainer) {

        TrainerEntity toUpdate = mapper.mapToEntity(updateTrainer);
        TrainerEntity trainerInDB = trainerRepository.findBiId(updateTrainer.getId());

        toUpdate.setName(trainerInDB.getName());

        TrainerEntity updated = trainerRepository.update(toUpdate);
        TrainerFullDto updateDto = mapper.mapToDto(updated);
        System.out.println("Trainer updated");

        return updateDto;
    }

    @Override
    public void deleteById(Long id) {

        trainerRepository.deleteById(id);
        System.out.println("trainer deleted");

    }
}
