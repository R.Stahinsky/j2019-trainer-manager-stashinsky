package by.itstep.trainer.manager.stashinsky.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "trainer")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name = "last_name",nullable = false)
    private String lastName;

    @Column(name = "work_experience")
    private int workExperience;

    @Column(name = "achievements")
    private String achievements;

    @Column(name = "image_url")
    private String imgUrl;

    @Column(name = "email",nullable = false)
    private String email;

    @Column(name = "password",nullable = false)
    private String password;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "trainer")
    List<CommentEntity> commentList;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "trainer")
    List<AppointmentEntity> appointments;


}
