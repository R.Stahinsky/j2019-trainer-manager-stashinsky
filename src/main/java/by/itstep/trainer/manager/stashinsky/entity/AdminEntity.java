package by.itstep.trainer.manager.stashinsky.entity;

import by.itstep.trainer.manager.stashinsky.enums.Role;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "admin")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdminEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name = "last_name",nullable = false)
    private String lastName;

    @Column(name = "email",nullable = false)
    private String email;

    @Column(name = "role",nullable = false)
    private Role role;

    @Column(name = "password",nullable = false)
    private String password;

}
