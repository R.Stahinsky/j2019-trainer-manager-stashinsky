package by.itstep.trainer.manager.stashinsky.mapper;

import by.itstep.trainer.manager.stashinsky.dto.admin.AdminCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminUpdateDto;
import by.itstep.trainer.manager.stashinsky.entity.AdminEntity;

import java.util.ArrayList;
import java.util.List;


public class AdminEntityMapper {

    public AdminEntity mapToEntity(AdminCreateDto createDto) {

        AdminEntity admin = new AdminEntity();

        admin.setName(createDto.getName());
        admin.setLastName(createDto.getLastName());
        admin.setEmail(createDto.getEmail());
        admin.setPassword(createDto.getPassword());

        return admin;
    }

    public AdminEntity mapToEntity(AdminUpdateDto updateDto) {

        AdminEntity admin = new AdminEntity();

        admin.setId(updateDto.getId());
        admin.setName(updateDto.getName());
        admin.setLastName(updateDto.getLastName());
        admin.setEmail(updateDto.getEmail());
        admin.setPassword(updateDto.getPassword());

        return admin;
    }

    public List<AdminPreviewDto> mapToDtoList(List<AdminEntity> entityList) {

        List<AdminPreviewDto> dtoList = new ArrayList<>();

        for (AdminEntity admin : entityList) {

            AdminPreviewDto dto = new AdminPreviewDto();

            dto.setId(admin.getId());
            dto.setName(admin.getName());
            dto.setLastName(admin.getLastName());
            dto.setEmail(admin.getEmail());
            dto.setRole(admin.getRole());

            dtoList.add(dto);
        }

        return dtoList;
    }

    public AdminFullDto mapToDto(AdminEntity adminEntity) {

        if (adminEntity == null) {
            return null;
        } else {

            AdminFullDto admin = new AdminFullDto();

            admin.setId(adminEntity.getId());
            admin.setName(adminEntity.getName());
            admin.setLastName(adminEntity.getLastName());
            admin.setEmail(adminEntity.getEmail());
            admin.setRole(adminEntity.getRole());

            return admin;
        }
    }


}
