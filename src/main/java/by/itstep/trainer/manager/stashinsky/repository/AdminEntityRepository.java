package by.itstep.trainer.manager.stashinsky.repository;

import by.itstep.trainer.manager.stashinsky.entity.AdminEntity;

import java.util.List;

public interface AdminEntityRepository {

    AdminEntity findById (Long id);

    List<AdminEntity> findAll();

    List<AdminEntity> findByName (String name);

    List<AdminEntity> findByLastName (String lastName);

    List<AdminEntity> findByEmail (String email);

    AdminEntity create (AdminEntity adminEntity);

    AdminEntity update (AdminEntity adminEntity);

    void  deleteById (Long id);

    void deleteAll();

}
