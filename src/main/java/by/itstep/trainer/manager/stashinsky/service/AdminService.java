package by.itstep.trainer.manager.stashinsky.service;

import by.itstep.trainer.manager.stashinsky.dto.admin.AdminCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.admin.AdminUpdateDto;

import java.util.List;

public interface AdminService {

    List<AdminPreviewDto> findAll();

    AdminFullDto findById(Long id);

    AdminFullDto create(AdminCreateDto createAdmin);

    AdminFullDto update (AdminUpdateDto updateAdmin);

    void deleteById(Long id);


}
