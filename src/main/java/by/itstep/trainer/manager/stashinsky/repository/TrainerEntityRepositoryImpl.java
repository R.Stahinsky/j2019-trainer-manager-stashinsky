package by.itstep.trainer.manager.stashinsky.repository;

import by.itstep.trainer.manager.stashinsky.entity.AdminEntity;
import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;
import by.itstep.trainer.manager.stashinsky.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class TrainerEntityRepositoryImpl implements TrainerEntityRepository {
    @Override
    public TrainerEntity findBiId(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        TrainerEntity foundTrainerEntity = em.find(TrainerEntity.class, id);

        em.close();
        System.out.println("Found trainer: " + foundTrainerEntity);
        return foundTrainerEntity;
    }

    @Override
    public List<TrainerEntity> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<TrainerEntity> foundTrainer = em.createNativeQuery("SELECT *FROM trainer", TrainerEntity.class).getResultList();

        em.close();
        System.out.println("Foind: " + foundTrainer.size() + "trainers");

        return foundTrainer;
    }

    @Override
    public List<TrainerEntity> findByName(String name) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<TrainerEntity> foundByName = em.createNativeQuery("SELECT *FROM trainer WHERE name='" + name + "'",TrainerEntity.class).getResultList();

        em.close();
        System.out.println("Found by name: " + foundByName);
        return foundByName;
    }

    @Override
    public List<TrainerEntity> findByLastName(String lastName) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<TrainerEntity> foundByLastName = em.createNativeQuery("SELECT *FROM trainer WHERE last_name='" + lastName + "'",TrainerEntity.class).getResultList();

        em.close();
        System.out.println("Found by name: " + foundByLastName);
        return foundByLastName;
    }

    @Override
    public List<TrainerEntity> findByEmail(String email) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<TrainerEntity> foundByEmail = em.createNativeQuery("SELECT * FROM trainer WHERE email ='" + email + "'", TrainerEntity.class).getResultList();

        em.close();
        System.out.println("FOUND BY EMAIL: " + foundByEmail);
        return foundByEmail;    }

    @Override
    public List<TrainerEntity> findByExperience(int workExperience) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<TrainerEntity> foundByExperience = em.createNativeQuery("SELECT * FROM trainer WHERE work_experience ='"+workExperience+"'",TrainerEntity.class).getResultList();
        em.close();
        System.out.println("Found by experience: "+ foundByExperience);
        return foundByExperience;
    }

    @Override
    public TrainerEntity create(TrainerEntity trainerEntity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(trainerEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("trainer is created");
        return trainerEntity;
    }

    @Override
    public TrainerEntity update(TrainerEntity trainerEntity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(trainerEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("trainer is updated");
        return trainerEntity;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        TrainerEntity foundTrainer = em.find(TrainerEntity.class, id);
        em.remove(foundTrainer);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM trainer").executeUpdate();

        em.getTransaction().commit();
        em.close();
        System.out.println("all trainerss was deleted");


    }
}
