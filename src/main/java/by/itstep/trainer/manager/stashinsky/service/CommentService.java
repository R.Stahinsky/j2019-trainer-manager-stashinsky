package by.itstep.trainer.manager.stashinsky.service;

import by.itstep.trainer.manager.stashinsky.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentUpdateDto;

import java.util.List;

public interface CommentService {

    List<CommentPreviewDto> findAll();

    CommentFullDto findById (Long id);

    CommentFullDto create (CommentCreateDto createComment);

    CommentFullDto update (CommentUpdateDto updateComment);

    void deleteById (Long id);
}
