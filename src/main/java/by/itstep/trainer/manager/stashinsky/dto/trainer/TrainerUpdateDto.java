package by.itstep.trainer.manager.stashinsky.dto.trainer;

import lombok.Data;

@Data
public class TrainerUpdateDto {

    private Long id;
    private String name;
    private String lastName;
    private int workExperience;
    private String achievements;
    private String imgUrl;
    private String email;
    private String password;

}
