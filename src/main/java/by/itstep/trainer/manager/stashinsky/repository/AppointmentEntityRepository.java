package by.itstep.trainer.manager.stashinsky.repository;

import by.itstep.trainer.manager.stashinsky.entity.AppointmentEntity;

import java.util.List;

public interface AppointmentEntityRepository {

    List<AppointmentEntity> findAll();

    AppointmentEntity findById(Long id);

    List<AppointmentEntity> findByName(String name);

    List<AppointmentEntity> findByLastName(String lastName);

    List<AppointmentEntity> findByEmail(String email);

    List<AppointmentEntity> findByPhoneNumber(int phoneNumber);

    AppointmentEntity create(AppointmentEntity appointmentEntity);

    AppointmentEntity update (AppointmentEntity appointmentEntity);

    void deleteById (Long id);

    void deleteAll();
}
