package by.itstep.trainer.manager.stashinsky.dto.trainer;

import by.itstep.trainer.manager.stashinsky.entity.AppointmentEntity;
import by.itstep.trainer.manager.stashinsky.entity.CommentEntity;
import lombok.Data;

import java.util.List;

@Data
public class TrainerFullDto {
    private Long id;
    private String name;
    private String lastName;
    private int workExperience;
    private String achievements;
    private String imgUrl;
    private String email;
    private String password;
    private List<CommentEntity> commentList;
    private List<AppointmentEntity> appointmentList;

}
