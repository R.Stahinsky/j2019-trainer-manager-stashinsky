package by.itstep.trainer.manager.stashinsky.dto.admin;

import lombok.Data;

@Data
public class AdminCreateDto {

    private String name;
    private String lastName;
    private String email;
    private String password;
}
