package by.itstep.trainer.manager.stashinsky.mapper;

import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.stashinsky.entity.AppointmentEntity;
import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class AppointmentEntityMapper {

    public AppointmentEntity mapToEntity(AppointmentCreateDto createDto, TrainerEntity trainer) {

        AppointmentEntity appointment = new AppointmentEntity();

        appointment.setAppointmentDate(createDto.getAppointmentDate());
        appointment.setAppointmentTime(createDto.getAppointmentTime());
        appointment.setMessage(createDto.getMessage());
        appointment.setName(createDto.getName());
        appointment.setLastName(createDto.getLastName());
        appointment.setEmail(createDto.getEmail());
        appointment.setPhoneNumber(createDto.getPhoneNumber());
        appointment.setTrainer(trainer);

        return appointment;
    }

    public AppointmentEntity mapToEntity(AppointmentUpdateDto updateDto, TrainerEntity trainer) {

        AppointmentEntity appointment = new AppointmentEntity();

        appointment.setId(updateDto.getId());
        appointment.setAppointmentDate(updateDto.getAppointmentDate());
        appointment.setAppointmentTime(updateDto.getAppointmentTime());
        appointment.setMessage(updateDto.getMessage());
        appointment.setName(updateDto.getName());
        appointment.setLastName(updateDto.getLastName());
        appointment.setEmail(updateDto.getEmail());
        appointment.setPhoneNumber(updateDto.getPhoneNumber());
        appointment.setTrainer(trainer);

        return appointment;
    }

    public List<AppointmentPreviewDto> mapToDtoList(List<AppointmentEntity> entityList) {

        List<AppointmentPreviewDto> dtoList = new ArrayList<>();

        for (AppointmentEntity appointment : entityList) {

            AppointmentPreviewDto dto = new AppointmentPreviewDto();

            dto.setId(appointment.getId());
            dto.setAppointmentDate(appointment.getAppointmentDate());
            dto.setAppointmentTime(appointment.getAppointmentTime());
            dto.setName(appointment.getName());
            dto.setLastName(appointment.getLastName());
            dto.setMessage(appointment.getMessage());
            dto.setPhoneNumber(appointment.getPhoneNumber());

            dtoList.add(dto);
        }
        return dtoList;
    }

    public AppointmentFullDto mapToDto(AppointmentEntity appointmentEntity) {

        if (appointmentEntity == null) {
            return null;
        } else {

            AppointmentFullDto dto = new AppointmentFullDto();

            dto.setId(appointmentEntity.getId());
            dto.setAppointmentDate(appointmentEntity.getAppointmentDate());
            dto.setAppointmentTime(appointmentEntity.getAppointmentTime());
            dto.setMessage(appointmentEntity.getMessage());
            dto.setName(appointmentEntity.getName());
            dto.setLastName(appointmentEntity.getLastName());
            dto.setEmail(appointmentEntity.getEmail());
            dto.setPhoneNumber(appointmentEntity.getPhoneNumber());
            dto.setTrainer(appointmentEntity.getTrainer());


            return dto;
        }
    }
}
