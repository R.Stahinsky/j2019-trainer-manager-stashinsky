package by.itstep.trainer.manager.stashinsky.mapper;

import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class TrainerEntityMapper {

    public TrainerEntity mapToEntity (TrainerCreateDto createDto) {

        TrainerEntity trainer = new TrainerEntity();

        trainer.setName(createDto.getName());
        trainer.setLastName(createDto.getLastName());
        trainer.setWorkExperience(createDto.getWorkExperience());
        trainer.setAchievements(createDto.getAchievements());
        trainer.setImgUrl(createDto.getImgUrl());
        trainer.setEmail(createDto.getEmail());
        trainer.setPassword(createDto.getPassword());

        return trainer;
    }

    public TrainerEntity mapToEntity (TrainerUpdateDto update) {

        TrainerEntity trainer = new TrainerEntity();

        trainer.setId(update.getId());
        trainer.setName(update.getName());
        trainer.setLastName(update.getLastName());
        trainer.setWorkExperience(update.getWorkExperience());
        trainer.setAchievements(update.getAchievements());
        trainer.setImgUrl(update.getImgUrl());
        trainer.setEmail(update.getEmail());
        trainer.setPassword(update.getPassword());

        return trainer;
    }

    public List<TrainerPreviewDto> mapToDtoList (List<TrainerEntity> entityList) {

        List<TrainerPreviewDto> dtoList = new ArrayList<>();

        for (TrainerEntity trainer : entityList) {

            TrainerPreviewDto dto = new TrainerPreviewDto();

            dto.setId(trainer.getId());
            dto.setName(trainer.getName());
            dto.setLastName(trainer.getLastName());
            dto.setWorkExperience(trainer.getWorkExperience());
            dto.setAchievements(trainer.getAchievements());
            dto.setImgUrl(trainer.getImgUrl());

            dtoList.add(dto);
        }
        return dtoList;
    }

    public TrainerFullDto mapToDto (TrainerEntity trainerEntity){

        if (trainerEntity == null) {
            return null;
        } else {

            TrainerFullDto dto = new TrainerFullDto();

            dto.setId(trainerEntity.getId());
            dto.setName(trainerEntity.getName());
            dto.setLastName(trainerEntity.getLastName());
            dto.setWorkExperience(trainerEntity.getWorkExperience());
            dto.setAchievements(trainerEntity.getAchievements());
            dto.setImgUrl(trainerEntity.getImgUrl());
            dto.setEmail(trainerEntity.getEmail());
            dto.setPassword(trainerEntity.getPassword());
            dto.setCommentList(trainerEntity.getCommentList());
            dto.setAppointmentList(trainerEntity.getAppointments());

            return dto;
        }
    }
}
