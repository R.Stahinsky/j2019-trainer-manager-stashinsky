package by.itstep.trainer.manager.stashinsky.entity;

import by.itstep.trainer.manager.stashinsky.enums.CommentStatus;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "comment")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name",nullable = false)
    private String userName;

    @Column(name = "user_last_name",nullable = false)
    private String userLastName;

    @Column(name = "message",nullable = false)
    private String message;

    @Column(name = "email",nullable = false)
    private String email;

    @Column(name = "trainer_points")
    private int trainerPoints;

    @Column(name = "status",nullable = false)
    private CommentStatus status;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "trainer_id")
    private TrainerEntity trainer;
}
