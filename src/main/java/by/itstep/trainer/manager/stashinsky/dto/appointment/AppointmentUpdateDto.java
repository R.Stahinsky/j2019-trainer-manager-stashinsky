package by.itstep.trainer.manager.stashinsky.dto.appointment;

import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class AppointmentUpdateDto {

    private Long id;
    private LocalDate appointmentDate;
    private LocalTime appointmentTime;
    private String message;
    private String name;
    private String lastName;
    private String email;
    private int phoneNumber;
    private Long trainerId;
}
