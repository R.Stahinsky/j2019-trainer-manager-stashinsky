package by.itstep.trainer.manager.stashinsky.enums;

public enum CommentStatus {
    CHECKING,
    PUBLISHED,
    DELETE
}
