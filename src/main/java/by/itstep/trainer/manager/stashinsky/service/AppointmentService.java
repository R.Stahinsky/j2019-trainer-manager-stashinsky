package by.itstep.trainer.manager.stashinsky.service;

import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.appointment.AppointmentUpdateDto;

import java.util.List;

public interface AppointmentService {

    List<AppointmentPreviewDto> findAll();

    AppointmentFullDto findById(Long id);

    AppointmentFullDto create  (AppointmentCreateDto createAppointment);

    AppointmentFullDto update (AppointmentUpdateDto updateAppointment);

    void deleteById (Long id);
}
