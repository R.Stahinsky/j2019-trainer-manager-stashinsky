package by.itstep.trainer.manager.stashinsky.dto.comment;

import lombok.Data;

@Data
public class CommentUpdateDto {

    private Long id;
    private String message;
    private int trainerPoints;

}
