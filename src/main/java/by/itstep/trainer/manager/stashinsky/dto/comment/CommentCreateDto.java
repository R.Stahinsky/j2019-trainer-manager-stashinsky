package by.itstep.trainer.manager.stashinsky.dto.comment;

import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;
import lombok.Data;

@Data
public class CommentCreateDto {

    private String userName;
    private String userLastName;
    private String message;
    private int trainerPoints;
    private Long trainerId;
}
