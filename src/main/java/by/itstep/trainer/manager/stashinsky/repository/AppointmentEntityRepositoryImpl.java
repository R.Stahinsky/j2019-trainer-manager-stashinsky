package by.itstep.trainer.manager.stashinsky.repository;

import by.itstep.trainer.manager.stashinsky.entity.AdminEntity;
import by.itstep.trainer.manager.stashinsky.entity.AppointmentEntity;
import by.itstep.trainer.manager.stashinsky.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class AppointmentEntityRepositoryImpl implements AppointmentEntityRepository {

    @Override
    public AppointmentEntity findById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        AppointmentEntity foundAppointment = em.find(AppointmentEntity.class,id);

        em.close();
        System.out.println("FOUND APPOINTMENT: " +foundAppointment);
        return foundAppointment;
    }

    @Override
    public List<AppointmentEntity> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AppointmentEntity> foundAppointmentList = em.createNativeQuery("SELECT * FROM appointment",AppointmentEntity.class).getResultList();
        em.close();
        System.out.println("Found " + foundAppointmentList.size()+"appointments");
        return foundAppointmentList;
    }

    @Override
    public List<AppointmentEntity> findByName(String name) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AppointmentEntity> foundAppointmentByName = em.createNativeQuery("SELECT * FROM appointment WHERE name ='" + name + "'", AppointmentEntity.class).getResultList();

        em.close();
        System.out.println("FOUND BY NAME: " + foundAppointmentByName);
        return foundAppointmentByName;
    }

    @Override
    public List<AppointmentEntity> findByLastName(String lastName) {

        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AppointmentEntity> foundAppointmentByLastName = em.createNativeQuery("SELECT * FROM appointment WHERE last_name ='" + lastName + "'", AppointmentEntity.class).getResultList();

        em.close();
        System.out.println("FOUND BY LASTNAME: " + foundAppointmentByLastName);
        return foundAppointmentByLastName;
    }

    @Override
    public List<AppointmentEntity> findByEmail(String email) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AppointmentEntity> foundAppointmentByEmail = em.createNativeQuery("SELECT * FROM appointment WHERE email ='" + email + "'", AppointmentEntity.class).getResultList();

        em.close();
        System.out.println("FOUND BY EMAIL: " + foundAppointmentByEmail);
        return foundAppointmentByEmail;
    }

    @Override
    public List<AppointmentEntity> findByPhoneNumber(int phoneNumber) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<AppointmentEntity> foundAppointmentByPhone = em.createNativeQuery("SELECT * FROM appointment WHERE phone_number ='"+phoneNumber+"'",AppointmentEntity.class).getResultList();
        em.close();
        System.out.println("FOUND BY PHONE: " + foundAppointmentByPhone);
        return foundAppointmentByPhone;
    }

    @Override
    public AppointmentEntity create(AppointmentEntity appointmentEntity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(appointmentEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Appointment was created");
        return appointmentEntity;
    }

    @Override
    public AppointmentEntity update(AppointmentEntity appointmentEntity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(appointmentEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Appointment was updated");
        return appointmentEntity;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        AppointmentEntity foundAppointmentForDelete = em.find(AppointmentEntity.class,id);
        em.remove(foundAppointmentForDelete);

        em.getTransaction().commit();
        em.close();
        System.out.println("Appointment was deleted");
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM appointment").executeUpdate();

        em.getTransaction().commit();
        em.close();
        System.out.println("all appointments was deleted");


    }
}
