package by.itstep.trainer.manager.stashinsky.repository;

import by.itstep.trainer.manager.stashinsky.entity.CommentEntity;

import java.util.List;

public interface CommentEntityRepository {

    CommentEntity findById (Long id);

    List<CommentEntity> findAll ();

    List<CommentEntity> findByUserName (String userName);

    List< CommentEntity> findByUserLastName (String userLastName);

    List<CommentEntity> findByEmail (String email);

    CommentEntity create (CommentEntity commentEntity);

    CommentEntity update (CommentEntity commentEntity);

    void deleteById (Long id);

    void deleteAll();

}
