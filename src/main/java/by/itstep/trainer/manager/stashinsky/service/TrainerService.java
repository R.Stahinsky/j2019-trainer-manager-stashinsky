package by.itstep.trainer.manager.stashinsky.service;

import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.trainer.TrainerUpdateDto;

import java.util.List;

public interface TrainerService {

    List<TrainerPreviewDto> findAll();

    TrainerFullDto findById (Long id);

    TrainerFullDto create (TrainerCreateDto createTrainer);

    TrainerFullDto update (TrainerUpdateDto updateTrainer);

    void deleteById (Long id) ;
}
