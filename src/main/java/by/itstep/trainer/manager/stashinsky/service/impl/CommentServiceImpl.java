package by.itstep.trainer.manager.stashinsky.service.impl;

import by.itstep.trainer.manager.stashinsky.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.stashinsky.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.stashinsky.entity.CommentEntity;
import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;
import by.itstep.trainer.manager.stashinsky.mapper.CommentEntityMapper;
import by.itstep.trainer.manager.stashinsky.repository.CommentEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.CommentEntityRepositoryImpl;
import by.itstep.trainer.manager.stashinsky.repository.TrainerEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.TrainerEntityRepositoryImpl;
import by.itstep.trainer.manager.stashinsky.service.CommentService;

import java.util.List;

public class CommentServiceImpl implements CommentService {

    private final CommentEntityRepository commentRepository = new CommentEntityRepositoryImpl();
    private final CommentEntityMapper mapper = new CommentEntityMapper();
    private final TrainerEntityRepository trainerRepository = new TrainerEntityRepositoryImpl();
    @Override
    public List<CommentPreviewDto> findAll() {

        List<CommentPreviewDto> dtos = mapper.mapToDtoList(commentRepository.findAll());
        System.out.println("Comments found");

        return dtos;
    }

    @Override
    public CommentFullDto findById(Long id){

        CommentFullDto comment = mapper.mapToDto(commentRepository.findById(id));

        System.out.println("Comment: " + comment);

    return comment;
    }

    @Override
    public CommentFullDto create(CommentCreateDto createComment) {

        TrainerEntity trainer = trainerRepository.findBiId(createComment.getTrainerId());

        CommentEntity commentToSave = mapper.mapToEntity(createComment,trainer);
        CommentEntity created = commentRepository.create(commentToSave);

        CommentFullDto createDto = mapper.mapToDto(created);
        System.out.println("Comment created");

        return createDto;
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateComment) {

        CommentEntity toUpdate = mapper.mapToEntity(updateComment);
        CommentEntity commentInDB = commentRepository.findById(updateComment.getId());

        toUpdate.setMessage(commentInDB.getMessage());

        CommentEntity updated = commentRepository.update(toUpdate);
        CommentFullDto updateDto = mapper.mapToDto(updated);
        System.out.println("Comment was pdated");

        return updateDto;
    }

    @Override
    public void deleteById(Long id) {

        commentRepository.deleteById(id);
        System.out.println("Comment was deleted");


    }
}
