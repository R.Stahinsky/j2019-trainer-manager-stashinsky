package by.itstep.trainer.manager.stashinsky.dto.appointment;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class AppointmentPreviewDto {

    private Long id;
    private LocalDate appointmentDate;
    private LocalTime appointmentTime;
    private String name;
    private String lastName;
    private String message;
    private int phoneNumber;


}
