package by.itstep.trainer.manager.stashinsky.dto.admin;

import lombok.Data;

@Data
public class AdminUpdateDto {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private String password;
}
