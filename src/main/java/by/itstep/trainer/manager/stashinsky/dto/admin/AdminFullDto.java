package by.itstep.trainer.manager.stashinsky.dto.admin;

import by.itstep.trainer.manager.stashinsky.enums.Role;
import lombok.Data;

@Data
public class AdminFullDto {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private Role role;





}
