package by.itstep.trainer.manager.stashinsky;

import by.itstep.trainer.manager.stashinsky.entity.AdminEntity;
import by.itstep.trainer.manager.stashinsky.repository.AdminEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.AdminEntityRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import lombok.Data;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.trainer.manager.stashinsky.enums.Role.ADMIN;
import static by.itstep.trainer.manager.stashinsky.enums.Role.SUPERUSER;

@SpringBootTest
class AdminTests {

    private AdminEntityRepository repository = new AdminEntityRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void testCreate() {
        //given
        AdminEntity admin = AdminEntity.builder()
                .name("name")
                .lastName("grfge")
                .email("gwrw")
                .password("rwrg")
                .role(SUPERUSER)
                .build();
        //when
        AdminEntity saved = repository.create(admin);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll() {
        //given
        AdminEntity admin1 = AdminEntity.builder()
                .name("sam")
                .lastName("jones")
                .email("gwrw@fef")
                .password("rwrg")
                .role(ADMIN)
                .build();
        AdminEntity admin2 = AdminEntity.builder()
                .name("ann")
                .lastName("kirk")
                .email("123@f234f")
                .password("414r")
                .role(ADMIN)
                .build();
        repository.create(admin1);
        repository.create(admin2);

        //when
        List<AdminEntity> foundAdmins = repository.findAll();
        System.out.println(foundAdmins);

        //then
        Assertions.assertEquals(2, foundAdmins.size());
    }

    @Test
    void testFindById() {
        //given
        AdminEntity admin = AdminEntity.builder()
                .name("JACK")
                .lastName("RED")
                .email("123@f234f")
                .password("414r")
                .role(ADMIN)
                .build();
        AdminEntity admin2 = AdminEntity.builder()
                .name("ann")
                .lastName("kirk")
                .email("123@f234f")
                .password("414r")
                .role(ADMIN)
                .build();

        repository.create(admin);
        repository.create(admin2);

        //when
        AdminEntity founded = repository.findById(admin2.getId());

        //then
        Assertions.assertNotNull(founded);
    }

    @Test
    void testFindByName() {
        //given
        AdminEntity admin = AdminEntity.builder()
                .name("JACK")
                .lastName("RED")
                .email("123@f234f")
                .password("414r")
                .role(ADMIN)
                .build();
        AdminEntity admin2 = AdminEntity.builder()
                .name("ANN")
                .lastName("kirk")
                .email("123@f234f")
                .password("414r")
                .role(ADMIN)
                .build();
        AdminEntity admin3 = AdminEntity.builder()
                .name("ANN")
                .lastName("SAMUEL")
                .email("@99900000")
                .password("414r")
                .role(SUPERUSER)
                .build();
        repository.create(admin);
        repository.create(admin2);
        repository.create(admin3);

        //when
        List<AdminEntity> response = repository.findByName("ANN");

        //then
        Assertions.assertNotNull(response);
    }

    @Test
    void testFindByLastName() {
        //given
        AdminEntity admin = AdminEntity.builder()
                .name("JACK")
                .lastName("RED")
                .email("123@f234f")
                .password("414r")
                .role(ADMIN)
                .build();
        AdminEntity admin2 = AdminEntity.builder()
                .name("ANN")
                .lastName("kirk")
                .email("123@f234f")
                .password("414r")
                .role(SUPERUSER)
                .build();
        AdminEntity admin3 = AdminEntity.builder()
                .name("Seth")
                .lastName("RED")
                .email("123@f234f")
                .password("414r")
                .role(SUPERUSER)
                .build();

        repository.create(admin);
        repository.create(admin2);
        repository.create(admin3);

        //when
        List<AdminEntity> response = repository.findByLastName("RED");

        //then
        Assertions.assertNotNull(response);
    }

    @Test
    void testFindByEmail() {
        //given
        AdminEntity admin1 = AdminEntity.builder()
                .name("sam")
                .lastName("jones")
                .email("testedmail@gmail.com")
                .password("rwrg")
                .role(ADMIN)
                .build();
        AdminEntity admin2 = AdminEntity.builder()
                .name("ANN")
                .lastName("kirk")
                .email("123@f234f")
                .password("414r")
                .role(ADMIN)
                .build();
        AdminEntity admin3 = AdminEntity.builder()
                .name("ANN")
                .lastName("SAMUEL")
                .email("@99900000")
                .password("414r")
                .role(SUPERUSER)
                .build();
        repository.create(admin1);
        repository.create(admin2);
        repository.create(admin3);

        //when
        List<AdminEntity> response = repository.findByEmail("testedmail@gmail.com");

        //then
        Assertions.assertNotNull(response);
    }

    @Test
    void testUpdate() {
        //given
        AdminEntity admin1 = AdminEntity.builder()
                .name("sam")
                .lastName("jones")
                .email("testedmail@gmail.com")
                .password("rwrg")
                .role(ADMIN)
                .build();

        AdminEntity admin2 = AdminEntity.builder()
                .id(admin1.getId())
                .name("ANN")
                .lastName("kirk")
                .email(admin1.getEmail())
                .password(admin1.getPassword())
                .role(admin1.getRole())
                .build();

        repository.create(admin1);
        System.out.println("FIRST VERSION OF ADMIN: " + admin1);

        //when

        AdminEntity adminForUpdate = repository.update(admin2);
        System.out.println(adminForUpdate);

        //then
        Assertions.assertEquals(admin2.getName(), adminForUpdate.getName());
    }

    @Test
    void testDeleteById() {
        AdminEntity admin1 = AdminEntity.builder()
                .name("sam")
                .lastName("jones")
                .email("testedmail@gmail.com")
                .password("rwrg")
                .role(ADMIN)
                .build();

        AdminEntity adminForDelete = repository.create(admin1);
        //when
        repository.deleteById(admin1.getId());
        //then

    }
}
