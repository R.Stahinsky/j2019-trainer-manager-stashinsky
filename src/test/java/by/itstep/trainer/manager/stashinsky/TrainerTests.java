package by.itstep.trainer.manager.stashinsky;

import by.itstep.trainer.manager.stashinsky.entity.TrainerEntity;
import by.itstep.trainer.manager.stashinsky.repository.TrainerEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.TrainerEntityRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class TrainerTests {

    TrainerEntityRepository repository = new TrainerEntityRepositoryImpl();

    @BeforeEach
    void setUp (){
        repository.deleteAll();
    }

    @Test
    void testCreate(){
        //given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .name("Casey")
                .lastName("Neistat")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        //when
        TrainerEntity create = repository.create(trainer1);

        //then
        Assertions.assertNotNull(create.getId());
    }

    @Test
    void testFindAll(){
        //given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .name("Casey")
                .lastName("Neistat")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        TrainerEntity trainer2 = TrainerEntity.builder()
                .name("Jack")
                .lastName("Boom")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();
        repository.create(trainer1);
        repository.create(trainer2);

        //when
        List<TrainerEntity> foundTrainers = repository.findAll();

        //then
        Assertions.assertEquals(2,foundTrainers.size());
    }

    @Test
    void testFindById (){
        //given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .name("Casey")
                .lastName("Neistat")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        TrainerEntity trainer2 = TrainerEntity.builder()
                .name("Jack")
                .lastName("Boom")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();
        repository.create(trainer1);
        repository.create(trainer2);

        //when
        TrainerEntity founById = repository.findBiId(trainer2.getId());

        //then
        Assertions.assertNotNull(founById);
    }

    @Test
    void testFindByName (){
        //given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .name("Casey")
                .lastName("Neistat")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        TrainerEntity trainer2 = TrainerEntity.builder()
                .name("Jack")
                .lastName("Boom")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        TrainerEntity trainer3 = TrainerEntity.builder()
                .name("Jack")
                .lastName("Boom")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();
        repository.create(trainer1);
        repository.create(trainer2);
        repository.create(trainer3);

        //when
        List<TrainerEntity> foundByName = repository.findByName(trainer2.getName());

        //then
        Assertions.assertNotNull(foundByName);
    }

    @Test
    void testFindByLastName (){
        //given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .name("Casey")
                .lastName("Neistat")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        TrainerEntity trainer2 = TrainerEntity.builder()
                .name("Jack")
                .lastName("Boom")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        TrainerEntity trainer3 = TrainerEntity.builder()
                .name("Jack")
                .lastName("Boom")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();
        repository.create(trainer1);
        repository.create(trainer2);
        repository.create(trainer3);

        //when
        List<TrainerEntity> foundByLastName = repository.findByLastName(trainer2.getLastName());

        //then
        Assertions.assertNotNull(foundByLastName);
    }


    @Test
    void testFindByExperience (){
        //given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .name("Casey")
                .lastName("Neistat")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        TrainerEntity trainer2 = TrainerEntity.builder()
                .name("Jack")
                .lastName("Boom")
                .workExperience(2)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();
        repository.create(trainer1);
        repository.create(trainer2);

        //when
        List<TrainerEntity> foundByExperience = repository.findByExperience(trainer2.getWorkExperience());

        //then
        Assertions.assertNotNull(foundByExperience);
    }

    @Test
    void testUpdate (){
        //given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .name("Casey")
                .lastName("Neistat")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        TrainerEntity trainer2 = TrainerEntity.builder()
                .id(trainer1.getId())
                .name("Jack")
                .lastName("Boom")
                .workExperience(2)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();
        repository.create(trainer1);

        //when
        TrainerEntity trainerForUpdate = repository.update(trainer2);

        //then
        Assertions.assertEquals(trainer2.getName(),trainerForUpdate.getName());
    }

    @Test
    void deleteById (){
        //given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .name("Casey")
                .lastName("Neistat")
                .workExperience(4)
                .achievements("Master")
                .imgUrl("wgwg")
                .email("frwg@rrt")
                .password("23f2g4")
                .build();

        repository.create(trainer1);


        //when
        repository.deleteById(trainer1.getId());

    }
}
