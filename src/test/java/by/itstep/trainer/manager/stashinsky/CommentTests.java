package by.itstep.trainer.manager.stashinsky;

import by.itstep.trainer.manager.stashinsky.entity.CommentEntity;
import by.itstep.trainer.manager.stashinsky.repository.CommentEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.CommentEntityRepositoryImpl;
import org.apache.catalina.LifecycleState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.trainer.manager.stashinsky.enums.CommentStatus.CHECKING;

@SpringBootTest
class CommentTests {

    private CommentEntityRepository repository = new CommentEntityRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void testCreate() {
        //given
        CommentEntity comment1 = CommentEntity.builder()
                .userName("Victoria")
                .userLastName("Busina")
                .message("Hello")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        //when
        CommentEntity create = repository.create(comment1);

        //then
        Assertions.assertNotNull(create.getId());
    }

    @Test
    void testFindAll() {
        //given
        CommentEntity comment1 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();
        repository.create(comment1);
        repository.create(comment2);

        //when
        List<CommentEntity> foundComments = repository.findAll();

        //then
        Assertions.assertEquals(2, foundComments.size());

    }

    @Test
    void testFindById() {
        //given
        CommentEntity comment1 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();
        repository.create(comment1);
        repository.create(comment2);
        //when
        CommentEntity foundById = repository.findById(comment2.getId());

        //then
        Assertions.assertNotNull(foundById);
    }

    @Test
    void testFindByUserName() {
        //given
        CommentEntity comment1 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment3 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        repository.create(comment1);
        repository.create(comment2);
        repository.create(comment3);

        //when
        List<CommentEntity> foundByUserName = repository.findByUserName(comment2.getUserName());

        //then
        Assertions.assertNotNull(foundByUserName);
    }

    @Test
    void testFindByUserLastName() {
        //given
        CommentEntity comment1 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Lee")
                .message("Moon")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        repository.create(comment1);
        repository.create(comment2);

        //when
        List<CommentEntity> foundByUserLastName = repository.findByUserLastName(comment2.getUserLastName());

        //then
        Assertions.assertNotNull(foundByUserLastName);
    }

    @Test
    void testFindByEmail() {
        //given
        CommentEntity comment1 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment3 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("e4egw4t1##")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        repository.create(comment1);
        repository.create(comment2);
        repository.create(comment3);

        //when
        List<CommentEntity> foundByEmail = repository.findByEmail(comment2.getEmail());

        //then
        Assertions.assertNotNull(foundByEmail);
    }

    @Test
    void testUpdate() {
        //given
        CommentEntity comment1 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .id(comment1.getId())
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        repository.create(comment1);

        //when
        CommentEntity commentToUpdate = repository.update(comment2);

        //then
        Assertions.assertEquals(comment2.getUserName(), commentToUpdate.getUserName());

    }

    @Test
    void testDeleteById() {
        //given
        CommentEntity comment1 = CommentEntity.builder()
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .id(comment1.getId())
                .userName("Jack")
                .userLastName("Mortal")
                .message("Boo")
                .email("erer@fee")
                .trainerPoints(1000)
                .status(CHECKING)
                .build();

        repository.create(comment1);
        repository.create(comment2);

        //when
        repository.deleteById(comment1.getId());

        //then
    }

}
