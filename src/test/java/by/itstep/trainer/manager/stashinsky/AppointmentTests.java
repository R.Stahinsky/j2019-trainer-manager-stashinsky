package by.itstep.trainer.manager.stashinsky;

import by.itstep.trainer.manager.stashinsky.entity.AppointmentEntity;
import by.itstep.trainer.manager.stashinsky.repository.AppointmentEntityRepository;
import by.itstep.trainer.manager.stashinsky.repository.AppointmentEntityRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;


@SpringBootTest
class AppointmentTests {
    private AppointmentEntityRepository repository = new AppointmentEntityRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void testCreate() {
        //given
        AppointmentEntity appointment = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("some message")
                .name("Jack")
                .lastName("Berton")
                .email("wow@gmail.com")
                .phoneNumber(2323234)
                .build();
        //when
        AppointmentEntity create = repository.create(appointment);
        System.out.println("created appointment: " + create);
        //then
        Assertions.assertNotNull(create.getId());
    }

    @Test
    void testFindAll() {
        //given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("roworworow")
                .name("Sam")
                .lastName("Clinton")
                .email("wow@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("roworworow")
                .name("Nick")
                .lastName("Mike")
                .email("wow@gmail.com")
                .phoneNumber(2323234)
                .build();
        repository.create(appointment1);
        repository.create(appointment2);

        //when
        List<AppointmentEntity> foundAppointment = repository.findAll();
        System.out.println("Found: " + foundAppointment);

        //then
        Assertions.assertEquals(2, foundAppointment.size());
    }

    @Test
    void testFindById() {
        //given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("roworworow")
                .name("Sam")
                .lastName("Clinton")
                .email("wow@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("roworworow")
                .name("Nick")
                .lastName("Mike")
                .email("wow@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment3 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("tomorrow")
                .name("Nolan")
                .lastName("Batman")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();
        repository.create(appointment1);
        repository.create(appointment2);
        repository.create(appointment3);

        //when
        AppointmentEntity foundAppointment = repository.findById(appointment2.getId());
        System.out.println(foundAppointment);

        //then
        Assertions.assertNotNull(foundAppointment);
    }

    @Test
    void testFindByName() {
        //given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("Space")
                .name("JACK")
                .lastName("kirk")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("Space")
                .name("ANN")
                .lastName("SAMUEL")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();

        repository.create(appointment1);
        repository.create(appointment2);

        //when
        List<AppointmentEntity> foundByName = repository.findByName(appointment1.getName());
        System.out.println("Found: " + foundByName);
        //then
        Assertions.assertNotNull(foundByName);
    }

    @Test
    void findByLastName() {
        //given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("roworworow")
                .name("Bob")
                .lastName("Clinton")
                .email("wow@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("roworworow")
                .name("Sam")
                .lastName("John")
                .email("wow@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment3 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("dfer")
                .name("mike")
                .lastName("John")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();
        repository.create(appointment1);
        repository.create(appointment2);
        repository.create(appointment3);

        //when
        List<AppointmentEntity> foundByName = repository.findByLastName(appointment3.getLastName());

        //then
        Assertions.assertNotNull(foundByName);
    }

    @Test
    void findByEmail(){
        //given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("dfer")
                .name("mike")
                .lastName("John")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("roworworow")
                .name("Sam")
                .lastName("Clinton")
                .email("wow@gmail.com")
                .phoneNumber(2323234)
                .build();

        repository.create(appointment1);
        repository.create(appointment2);
        //when
        List<AppointmentEntity> foundByEmail = repository.findByEmail(appointment1.getEmail());

        //then
        Assertions.assertNotNull(foundByEmail);
    }

    @Test
    void testFindByPhoneNumber (){
        //given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("Space")
                .name("JACK")
                .lastName("kirk")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("Space")
                .name("ANN")
                .lastName("SAMUEL")
                .email("Bat@gmail.com")
                .phoneNumber(123)
                .build();

        repository.create(appointment1);
        repository.create(appointment2);

        //when
        List<AppointmentEntity> foundByNumber = repository.findByPhoneNumber(appointment2.getPhoneNumber());

        //then
        Assertions.assertNotNull(foundByNumber);
    }

    @Test
    void testUpdate (){
        //given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("Space")
                .name("JACK")
                .lastName("kirk")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .id(appointment1.getId())
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("Space")
                .name("ANN")
                .lastName("SAMUEL")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();

        repository.create(appointment1);
        //when
        AppointmentEntity appointmentForUpdate = repository.update(appointment2);

        //then
        Assertions.assertEquals(appointment2.getName(),appointmentForUpdate.getName());
    }

    @Test
    void testDeleteById(){
        //given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .appointmentDate(LocalDate.now())
                .appointmentTime(LocalTime.now())
                .message("Space")
                .name("JACK")
                .lastName("kirk")
                .email("Bat@gmail.com")
                .phoneNumber(2323234)
                .build();
        repository.create(appointment1);
        //when
        repository.deleteById(appointment1.getId());
        //then

    }
}
